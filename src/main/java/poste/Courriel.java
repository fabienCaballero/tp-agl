package poste;

import java.util.ArrayList;

public class Courriel {
	private String adrCourriel;
	private String Titre;
	private String corpsM;
	private ArrayList<PieceJointe> PieceJ;
	
	
	
	public Courriel(String adrCourriel, String titre, String corpsM, ArrayList<PieceJointe> pieceJ) {
		super();
		this.adrCourriel = adrCourriel;
		Titre = titre;
		this.corpsM = corpsM;
		PieceJ = pieceJ;
	}



	public String getAdrCourriel() {
		return adrCourriel;
	}



	public void setAdrCourriel(String adrCourriel) {
		this.adrCourriel = adrCourriel;
	}



	public String getTitre() {
		return Titre;
	}



	public void setTitre(String titre) {
		Titre = titre;
	}



	public String getCorpsM() {
		return corpsM;
	}



	public void setCorpsM(String corpsM) {
		this.corpsM = corpsM;
	}



	public ArrayList<PieceJointe> getPieceJ() {
		return PieceJ;
	}



	public void setPieceJ(ArrayList<PieceJointe> pieceJ) {
		PieceJ = pieceJ;
	}
	
	public boolean verifMail() {
		if(adrCourriel.matches("[a-zA-Z]{1}\\w+@\\w+[.]\\w+")) {
			return true;
		}
		else return false;
	}
	
	public boolean verifTitre() {
		if(Titre.compareTo("")==0 || Titre ==null) {
			return false;
		}
		else return true;
	}
	
	public boolean verifCorpsPJ() {
		return corpsM.matches("PJ|joint|jointe");
		
	}



	public void envoyer() {};

}

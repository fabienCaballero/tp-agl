import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import poste.Courriel;
import poste.PieceJointe;

public class TestCourrier {

/*	
	@ParameterizedTest
	@ValueSource(strings = { "orange@gmail.com", "Oo@ui.fr","az1de5@gmail.com"})
	public void verifMailsV(String mail) {
		Courriel a=new Courriel(mail,"gsyza","jointe",new ArrayList<PieceJointe>());
		assertEquals(a.verifMail(),true);
		}
	
	@ParameterizedTest
	@ValueSource(strings = { "1orange@gmail.com", "oo@ui", "d145@46954.","@gmail.fr","qqcj@.fr"})
	public void verifMailsF(String mail) {
		Courriel a=new Courriel(mail,"gsyza","jointe",new ArrayList<PieceJointe>());
		assertEquals(a.verifMail(),false,"le mail ne correspond pas à la regex");
		}
	*/
	private static Stream <Arguments > ParamsMail () {
		return Stream.of(
		Arguments.of("orange@gmail.com",true, ""),
		Arguments.of("Oo@ui.fr",true, ""),
		Arguments.of("az1de5@gmail.com",true, ""),
		Arguments.of("1orange@gmail.com",false, "chiffre en premiere position"),
		Arguments.of("oo@ui",false, "pas de ."),
		Arguments.of("d145@46954.",false, "pas de suite après le point"),
		Arguments.of("qqcj@.fr",false, "pas de caractere entre @ et point"),
		Arguments.of("@gmail.fr",false, "pas de caractères avant @")
);
		}
	@ParameterizedTest(name = "{index} : mail ={0}, validité ={1},raison={2}")
	@MethodSource("ParamsMail")
	public void verifMail(String mail,boolean validite, String raison) {
		Courriel c=new Courriel(mail,"T1","PJ",new ArrayList<PieceJointe>());
		assertEquals(c.verifMail(),validite);
	}

	@ParameterizedTest
	@ValueSource(strings = { "T1", "gjh", "T2" })
	public void verifTitresV(String titres) {
		Courriel t=new Courriel("mail@orange.fr",titres,"jointe",new ArrayList<PieceJointe>());
		assertEquals(t.verifTitre(),true);
	}
	@ParameterizedTest
	@ValueSource(strings = { "" })
	public void verifTitresF(String titres) {
		Courriel t=new Courriel("mail@orange.fr",titres,"jointe",new ArrayList<PieceJointe>());
		assertEquals(t.verifTitre(),false,"le titre ne doit pas être vide");
	}
	
	private static Stream <Arguments > ParamsCorps () {
		return Stream.of(
		Arguments.of("jointe",true, ""),
		Arguments.of("PJ",true, ""),
		Arguments.of("joint",true, ""),
		Arguments.of("jointes",false, "jointe ne possède pas de s"),
		Arguments.of("daqhgthy",false, "daqhgthy ne correspond pas à jointe ,ou PJ , ou joint"),
		Arguments.of("",false, "le corps ne doit pas être vide")

		);
		}
	
	@ParameterizedTest(name = "{index} : corps ={0}, validité ={1},raison={2}")
	@MethodSource("ParamsCorps")
	public void verifCorps(String corps,boolean correct, String raison) {
		Courriel c=new Courriel("mail@orange.fr","T1",corps,new ArrayList<PieceJointe>());
		assertEquals(c.verifCorpsPJ(),correct);
	}
	
	@ParameterizedTest
	@ValueSource(strings = { "jointe","PJ","joint"})
	public void verifCorpsV(String corps) {
		Courriel c=new Courriel("mail@orange.fr","T1",corps,new ArrayList<PieceJointe>());
		assertEquals(c.verifCorpsPJ(),true);
		
	}
	@ParameterizedTest
	@ValueSource(strings = { "jointes", "daqhgthy",""})
	public void verifCorpsF(String corps) {
		Courriel c=new Courriel("mail@orange.fr","T1",corps,new ArrayList<PieceJointe>());
		assertEquals(c.verifCorpsPJ(),false,"la piece jointe doit être : PJ ou joint, jointe");
		
	}
	
	
}

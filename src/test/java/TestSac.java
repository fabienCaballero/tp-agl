import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.*;

import poste.Colis;
import poste.Lettre;
import poste.Recommandation;
import poste.SacPostal;
 

public class TestSac {
	Lettre lettre1;
	Lettre lettre2;
	Colis colis1;
	SacPostal sac1;
	SacPostal sac2;
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;

	
	@BeforeEach	
	public void setUp() throws TestSacPostaux{
		sac1 = new SacPostal();
		lettre1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
		colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		
		sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
		sac1.ajoute(colis1);
		

		}
	
	@Test
	public void remboursementSac() {
	assertEquals(116.5f,sac1.valeurRemboursement(),tolerancePrix);
	}
	
	@Test
	public void toleranceSac1() {
		
	assertEquals(0.025359999558422715f,sac1.getVolume(),toleranceVolume);
	}
	
	@Test
	public void toleranceSac2() {
	sac2=sac1.extraireV1("7877");
	assertEquals(0.02517999955569394f,sac2.getVolume(),toleranceVolume);
	}
}